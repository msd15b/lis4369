#LIS4369 - Extensible Enterprise Solutions
## Matthew Dubin
###Project 2 Requirements

*Three Parts:*

1. Bakcward engineer from requirements
2. Chapter Questions
3. Bitbucket Repo Links:
 - [this assignment](https://bitbucket.org/msd15b/lis4369/src/master/p2/)

####Assignment Screenshots:
#####Screenshot of plot 1:
![p2](images/plot1.png)

#####Screenshot of plot 2:
![p2](images/plot2.png)
