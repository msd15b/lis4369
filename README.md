# LIS4369 - Extensible Enterprise Solutions

## Matthew Spenser Dubin

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "A1 README.md file")
    - Install R Studio
    - Install R
    - Provide screenshots of application
    - Create Bitbucket repo
    - Complete Bitbucket tutorial
        (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "A2 README.md file")
    - Backward Enginner Python Programs
    - Chapter Questions
    - Push to Bitbucket repo

3. [A3 README.md](a3/README.md "A3 README.md file")
    - Backward Enginner Python Programs
    - Chapter Questions
    - Push to Bitbucket repo

4. [P1 README.md](p1/README.md "P1 README.md file")
    - Install required packages
    - Python Program showing formatted data from ExxonMobile ticker
        - Main focus was gathering data using pandas, pandas_datareader, and producing a plot
    - Chapter Questions for chpater 7 and 8

5. [A4 README.md](a4/README.md "A4 README.md file")
    - Check if required packages are installed
    - Python Program showing formatted data from Titanic manifest
        - Main focus was gathering data using pandas, pandas_datareader, and producing a plot
    - Chapter Questions for chpater 9 and 10

6. [A5 README.md](a5/README.md "A5 README.md file")
    - Compelte Introduction to R Tutorial
    - Write and run a5.R as shown below
    - Include 2 plots produced in a5 below
    - Chapter questions from 11 and 12
    - R Commands file
    - R Console of commands executed
    - Graphs from Tutorial
    - RStudio Example

7. [P2 README.md](p2/README.md "P2 README.md file")
    - Backward engineer R from the requirements
    - Chapter Questions