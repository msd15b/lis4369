#!/usr/bin/env/python3

def main():
    get_requirements()
    print("Input:")
    hoursWorked = float(input("Enter hours worked: "))
    holidayHours = float(input("Enter holiday hours: "))
    hourRate = float(input("Enter hourly pay rate: "))
    calculate_payroll(hoursWorked, holidayHours, hourRate)


def calculate_payroll(hoursWorked, holidayHours, hourRate):
    baseHours = 40
    otRate = 1.5
    holidayRate = 2.0
    
    basePay = float(baseHours*hourRate)
    overtTimeHours = float(hoursWorked-baseHours)

    if hoursWorked > baseHours:
        overTime = overtTimeHours*hourRate*otRate
        holidayTime = holidayHours*hourRate*holidayRate
        totalPay = baseHours*hourRate+overTime+holidayTime
        printPay(basePay,overTime,holidayTime,totalPay)
    else:
        overTime = 0
        holidayTime = holidayHours*hourRate*holidayRate
        totalPay = hoursWorked*hourRate+holidayTime
        printPay(basePay,overTime,holidayTime,totalPay)

def printPay(basePay, overTime, holidayTime, totalPay):
    print("\nOutput:")
    print("Base:\t\t${0:,.02f}".format(basePay))
    print("Overtime:\t${0:,.02f}".format(overTime))
    print("Holiday:\t${0:,.02f}".format(holidayTime))
    print("Gross:\t\t${0:,.02f}".format(totalPay))

def get_requirements():
    print("Payroll Calculator\n\nProgram Requirements:")
    print("1. Must use float data type for user input.")
    print("2. Overtime rate: 1.5 times hourly rate (hours over 40).")
    print("3. Holiday rate: 2.0 times hourly rate (all holiday hours).")
    print("4. Must format currency with dollar sign, and roud to two decimal places.")
    print("5. Create at least three functions that are called by the program:")
    print("\ta. main(): calls at least two other functions.")
    print("\tb. get_requirements(): displays program requirements.")
    print("\tc. calculate_payroll(): calculates and individual one-week paycheck.")

if __name__ == "__main__":
    main()