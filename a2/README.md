#LIS4369 - Extensible Enterprise Solutions
## Matthew Dubin
###Assignment 2 Requirements

*Four Parts:*

1. Python Program without Overtime
2. Python Program with Overtime
3. Chapter Questions
4. Bitbucket Repo Links:
* [this assignment](https://bitbucket.org/msd15b/lis4369/src/master/a2/) and

####Assignment Screenshots:
#####Screenshot of a2_PayRoll_Calc:
![a2_PayRoll_Calc](images/NoOvertime.png)

#####Screenshot of a2_PayRoll_Calc:
![a2_PayRoll_Calc](images/Overtime.png)