#LIS4369 - Extensible Enterprise Solutions
## Matthew Dubin
###Assignment 1 Requirements

*Four Parts:*

1. Distributed Version Control with Bitbucket
2. Development Instalations
3. Bitbucket Repo Links:
* [this assignment](https://bitbucket.org/msd15b/lis4369/src/master/a1/) and
* [Bitbucket Station Locations](https://bitbucket.org/msd15b/bitbucketstationlocations/src/master/)

####README.md file should include the following items:
* Screenshot of a1_tip_calculator application running
* git commands with short descriptions
> This is a block quote.
> This is the second paragraph in the block quote.
>
> Git commands with short Descriptions
>

1.  git init - create new local repository
2.  git status - List the files you've changed and those you still need to add or commit
3.  git add - Add one or more files to staging (index)
4.  git commit - Stores the current contents of the index in a new commit along with a log message from the user
5.  git push - Send changes to the master branch of your remote repository
6.  git pull - Fetch and merge changes on the remote server to your working directory
7.  git branch - List all the branches in your repo, and also tell you what branch you're currently in

####Assignment Screenshots:
#####Screenshot of a1_tip_calculator running:
![a1_tip_calculator](images/terminal.png)

#####Screenshot of a1_tip_calculator running in VS Code:
![a1_tip_calculator](images/vscode.png)