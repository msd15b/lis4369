print("Tip Calculator\n")

print("Program Requirements:")
print('1. Must use float data type for user input (except,"Party Number").')
print("2. Must round calculations to two decimal places.")
print("3. Must format currency with dollar sign, and two decimal places.\n")

print("User input:")
mealCost = input("Cost of meal: ")
mealCostFloat = float(mealCost)
taxPerc = input("Tax Percent: ")
taxPercF = float(taxPerc)
tipPerc = input("Tip Percent: ")
tipPercF = float(tipPerc)
pNum = input("Party number: ")
pNumInt = int(pNum)

taxNum = taxPercF / 100
taxAmount = taxNum * mealCostFloat

amDue = mealCostFloat + taxAmount

tipNum = tipPercF / 100
tipAmount = tipNum * amDue

totalAmount = amDue + tipAmount
totalAmountSplit = totalAmount/pNumInt

print("\nProgram Output")

print("Subtotal:\t\t ${0:,.02f}".format(mealCostFloat))
print("Tax:\t\t\t ${0:,.02f}".format(taxAmount))
print("Amount Due:\t\t ${0:,.02f}".format(amDue))
print("Gratuity:\t\t ${0:,.02f}".format(tipAmount))
print("Total:\t\t\t ${0:,.02f}".format(totalAmount))
print("Split ("+ str(pNum) + "):\t\t ${0:,.02f}".format(totalAmountSplit))