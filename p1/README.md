#LIS4369 - Extensible Enterprise Solutions
## Matthew Dubin
###Project 1 Requirements

*Three Parts:*

1. Install required packages for program
2. Python Program showing formatted data from ExxonMobile ticker
   - Main focus was gathering data using pandas, pandas_datareader, and producing a plot
3. Chapter Questions for chpater 7 and 8
4. Bitbucket Repo Links:
 - [this assignment](https://bitbucket.org/msd15b/lis4369/src/master/p1/)

####Assignment Screenshots:
#####Screenshot of p1_demo.py:
![a3_painting_calculator](images/page1.png)

#####Screenshot of p1_demo.py page 2:
![a3_painting_calculator](images/page2.png)

#####Screenshot of plot graph from 
![a3_painting_calculator](images/plot.png)
