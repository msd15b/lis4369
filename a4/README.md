#LIS4369 - Extensible Enterprise Solutions
## Matthew Dubin
###Assignment 4 Requirements

*Three Parts:*

1. Python Program demo.py that uses the data from the Titanic
   - Main focus was python data manipulators and csv file reading
2. Chapter Questions 9, 10
3. Bitbucket Repo Links:
 - [this assignment](https://bitbucket.org/msd15b/lis4369/src/master/a4/)

####Assignment Screenshots:
#####Screenshot of demo.py requirements:
![demo.py](images/runtime.png)
