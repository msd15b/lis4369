#LIS4369 - Extensible Enterprise Solutions
## Matthew Dubin
###Assignment 5 Requirements

*Nine Parts:*

1. Compelte Introduction to R Tutorial
2. Write and run a5.R as shown below
3. Include 2 plots produced in a5 below
4. Chapter questions from 11 and 12
5. R Commands file
6. R Console of commands executed
7. Graphs from Tutorial
8. RStudio Example
9. Bitbucket Repo Links:
 - [this assignment](https://bitbucket.org/msd15b/lis4369/src/master/a5/)

####Assignment and Tutorial Screenshots:
#####Screenshot of a5.R runtime:
![Tutorial.py](images/a5_run.png)

#####Screenshot of a5.R plot 1:
![Tutorial.py](images/a5_graph1.png)

#####Screenshot of a5.R plot 2:
![Tutorial.py](images/a5_graph2.png)

#####Screenshot of R Console and commands:
![Tutorial.py](images/RStudio.png)

#####Screenshot of Graph 1 from Tutorial:
![Tutorial.py](images/tut_plot1.png)

#####Screenshot of Graph 2 from Tutorial:
![Tutorial.py](images/tut_plot2.png)

#####Screenshot of Graph 3 from Tutorial:
![Tutorial.py](images/tut_plot3.png)

#####Screenshot of Graph 4 from Tutorial:
![Tutorial.py](images/tut_plot4.png)

#####Screenshot of Graph 5 from Tutorial:
![Tutorial.py](images/tut_plot5.png)