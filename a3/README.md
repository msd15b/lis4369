#LIS4369 - Extensible Enterprise Solutions
## Matthew Dubin
###Assignment 3 Requirements

*Three Parts:*

1. Python Program showing multiple estimations
   - Main focus was using .formt() and being able to produce clean output
2. Chapter Questions
3. Bitbucket Repo Links:
 - [this assignment](https://bitbucket.org/msd15b/lis4369/src/master/a3/)

####Assignment Screenshots:
#####Screenshot of a3_painting_calculator.py:
![a3_painting_calculator](images/runtime.png)
