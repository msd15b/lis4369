#!/usr/bin/env/python3

def main():
    paintAnother = True
    areWePainting = ""
    SQFT_PER_GALLON = 350.0
    print("Painting Estimator\n")
    get_requirements()

    while paintAnother == True:
        print("\nInput:")
        interiorSqFt = float(input("Enter total interior sq ft: "))
        pricePerGallon = float(input("Enter price per gallon paint: "))
        paintingRateHour = float(input("Enter hourly painting rate per sq ft: "))
        estimate_painting_costs(SQFT_PER_GALLON, interiorSqFt, pricePerGallon, paintingRateHour)

        areWePainting = input("\nEstimate another paint job? (y/n): ")
        if areWePainting == "y":
            paintAnother = True
        if areWePainting == "n":
            paintAnother = False

    print("\nThank you for using our Painting Estimator!")
    print("Please see our web site: https://www.spenserdubin.com")


def estimate_painting_costs(SQFT_PER_GALLON, interiorSqFt, pricePerGallon, paintingRateHour):
    numberOfGallons = interiorSqFt / SQFT_PER_GALLON
    costForPaint = numberOfGallons * pricePerGallon
    costForLabor = paintingRateHour * interiorSqFt
    costTotal = costForLabor + costForPaint
    print_estimate(SQFT_PER_GALLON, interiorSqFt, costForPaint, costForLabor, costTotal, numberOfGallons, pricePerGallon, paintingRateHour)


def print_estimate(SQFT_PER_GALLON, interiorSqFt, costForPaint, costForLabor, costTotal, numberOfGallons, pricePerGallon, paintingRateHour):
    print("Output:\n")
    print("{:15} {:>12}".format("Item", "Amount"))
    print("{:15} {:>12,.2f}".format("Total Sq Ft: ", interiorSqFt))
    print("{:15} {:>10.2f}".format("Sq Ft per Gallon:", SQFT_PER_GALLON))
    print("{:15} {:>9.2f}".format("Number of Gallons:", numberOfGallons))
    print("{:15}   ${:>7.2f}".format("Paint per Gallon:", pricePerGallon))
    print("{:15}    ${:>7.2f}".format("Labor per Sq Ft:", paintingRateHour))
    print("\n{:10} {:10} {:10}".format("Cost", "Amount", "Percentage"))
    cPPerc = (costForPaint / costTotal)*100
    cLPerc = (costForLabor / costTotal)*100
    print("{:6}  ${:>8.2f} {:>13.2f}%".format("Paint:", costForPaint, cPPerc))
    print("{:6}  ${:>8.2f} {:>13.2f}%".format("Labor:", costForLabor, cLPerc))
    print("{:6}  ${:>8.2f} {:>13.2f}%".format("Total:", costTotal, cLPerc+cPPerc))

def get_requirements():
    print("\nProgram Requirements:")
    print("1. Calculate home interior paint cost (w/o primer).")
    print("2. Must use float data types.")
    print("3. Must use SQFT_PER_GALLON constant (350).")
    print("4. Must use iteration structure (aka loop).")
    print("5. Format, right-align numbers, and round to two decimal places.")
    print("6. Create at least three functions that are called by the program:")
    print("\ta. main(): calls at least two other functions.")
    print("\tb. get_requirements(): displays program requirements.")
    print("\tc. estimate_painting_costs(): calculates interior home painting.")

if __name__ == "__main__":
    main()